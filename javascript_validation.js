var validate = function(form,rules,lang=null) {
                /*
                    rules is:
                    - name/id
                    - required
                    - equals
                        -name/id
                        -label
                        -msg
                    - defaultValueValidation
                    - maxlength
                        - value
                        - msg
                    - minlength
                        - value
                        - msg
                    - filetypes
                        -extensions
                        -maxsize
                    - msg
                */

                var resvalidation = null;
                var itemval = {};
                var control = 0;
                var cantElements = form.elements.length;
                var elementForm, elementForm2;
                
                if(lang==null){
                    lang = {
                        msgradio : "Debe seleccionar una opción",
                        msgcheckbox : "Debe seleccionar almenos 1 opción",
                        msgselectone : "Debe seleccionar una opción",
                        msgnullordef : "Debe especificar un valor",
                        msgnullordefile : "Debe seleccionar un archivo",
                        msgminlength : "El rango de digitos es menor al requerido",
                        msgmaxlength : "El rango de digitos es mayor al requerido"
                    };
                }


                for(var i=0;i<=cantElements;i++){
                    elementForm = form[0].elements[i];
                    if(elementForm.id=='' || elementForm.id==0 || elementForm.id=='0'){
                        //validations
                        control = 1;

                        switch(elementForm.type){
                            
                            // radio buttons validation
                            case 'radio':
                                while(elementForm.type=="radio"){
                                    if(elementForm.checked==true){
                                        control = 0;
                                    }
                                    elementForm2=form.elements[(i+1)];
                                    if(elementForm2.type=="radio"){
                                        elementForm=elementForm2;
                                        ++i;
                                    }else{
                                        break;
                                    }
                                }
                                if(control==0){
                                    for (item in rules){
                                        if( (item['name']==elementForm.name
                                                || item['name']==elementForm.id) && (typeof item['required'] != undefined )){
                                            if(item['required']){
                                                resvalidation[ (typeof elementForm.id != undefined )?
                                                elementForm.id :
                                                elementForm.name ] = {
                                                    type: 'radio',
                                                    status: false,
                                                    msg: (typeof item['msg'] != undefined )? item['msg'] : lang['msgradio']
                                                };
                                            }
                                        }
                                    };
                                }
                                break;
                            
                            //chackbox buttons validation
                            case 'checkbox':
                                while(elementForm.type=="checkbox"){
                                    if(elementForm.checked==true){
                                        control = 0;
                                    }
                                    elementForm2=form.elements[(i+1)];
                                    if(elementForm2.type=="checkbox"){
                                        elementForm=elementForm2;
                                        ++i;
                                    }else{
                                        break;
                                    }
                                }
                                if(control==0){
                                    for (item in rules){
                                        if( (item['name']==elementForm.name
                                                || item['name']==elementForm.id) && (typeof item['required'] != undefined )){
                                            if(item['required']){
                                                resvalidation[ (typeof elementForm.id != undefined )?
                                                elementForm.id :
                                                elementForm.name ] = {
                                                    type: 'checkbox',
                                                    status: false,
                                                    msg: (typeof item['msg'] != undefined )? item['msg'] : lang["msgcheckbox"]
                                                };
                                            }
                                        }
                                    }
                                }
                                break;
                            
                            //validation text
                            case 'text':
                                var resval = txtvalidate(elementForm, rules, lang);
                                if(resval!=null)
                                    resvalidation[(typeof elementForm.id)?
                                                elementForm.id :
                                                elementForm.name] = resval;
				                break;
                            
                            //validation password
				            case 'password':
                                var resval = txtvalidate(elementForm, rules, lang);
                                if(resval!=null)
                                    resvalidation[(typeof elementForm.id)?
                                                elementForm.id :
                                                elementForm.name] = resval;
                                break;

                            //validation textarea
                            case 'textarea':
                                var resval = txtvalidate(elementForm, rules, lang);
                                    if(resval!=null)
                                        resvalidation[(typeof elementForm.id)?
                                                    elementForm.id :
                                                    elementForm.name] = resval;
                                break;
                            
                            //validate select-one tag
                            case 'select-one':
                                for (item in rules){
                                    if( (item['name']==elementForm.name
                                            || item['name']==elementForm.id) && (typeof item['required'] != undefined )){
                                        if(item['required']){
                                            if(elementForm.selectedIndex == 0){
                                                resvalidation[(typeof elementForm.id)?
                                                            elementForm.id :
                                                            elementForm.name] = {
                                                                type: 'select-one',
                                                                status: false,
                                                                msg: (typeof item['msg'] != undefined )? item['msg'] : lang["msgselectone"]
                                                            };
                                            }
                                        }
                                    }
                                }
                                break;

                            //validation file inputs
                            case 'file':

                                break;
                    }else{
                        continue;
                    }
                }
                return resvalidation;
            };

function filevalidate(elementForm, rules, lang){
    var itemval = null;

    for (item in rules){
        if(item['name']==elementForm.name || item['name']==elementForm.id){
            if(item["required"]){
                if(elementForm.value == null){
                    itemval={
                        type: 'file',
                        status: false,
                        msg: (typeof item['msg'] != undefined )? item['msg'] : lang["msgnullordefile"]
                    };
                    break;
                }else{
                    var filename = elementForm.value;
                    var ext = filename.replace(/^.*\./, '');               

                    if(ext == filename){
                        ext = '';
                    }else{
                        ext = extension.toLowerCase();
                        for(extval in item["extensions"] ){
                            if(ext!=extval){
                                itemval={
                                    type: 'file',
                                    status: false,
                                    msg: (typeof item['msg'] != undefined )? item['msg'] : lang["msgnullordef"]
                                };
                                break;
                            }
                        }
                        if(){
                            
                        }
                    }
                }
            }
        }
    }
    return itemval;
}

function txtvalidate(elementForm, rules, lang){
    var itemval = null;

    for (item in rules){
        if(item['name']==elementForm.name || item['name']==elementForm.id){
            if(item["required"]){
                if(typeof item["defaultValueValidation"] != undefined ){
                    if(elementForm.value==item["defaultValueValidation"]){
                        itemval={
                            type: 'text',
                            status: false,
                            msg: (typeof item['msg'] != undefined )? item['msg'] : lang["msgnullordef"]
                        };
                        break;
                    }else{
                        if(typeof item['minlength'])
                            if(elementForm.vale.length < item['minlength'])
                                itemval={
                                    type: 'text',
                                    status: false,
                                    msg: (typeof item['minlength']['msg'] != undefined)? item['minlength']['msg'] : lang["msgminlength"]
                                };
                                break;     
                        if(typeof item['maxlength'])
                            if(elementForm.vale.length > item['minlength'])
                                itemval={
                                    type: 'text',
                                    status: false,
                                    msg: (typeof item['maxlength']['msg'] != undefined )? item['maxlength']['msg'] : lang["msgmaxlength"]
                                };
                                break;
                    }
                }else{
                    if(elementForm.value=="" || elementForm.value==null){
                        itemval={
                            type: 'text',
                            status: false,
                            msg: (typeof item['msg'] != undefined )? item['msg'] : lang["msgnullordef"]
                        };
                        break;
                    }else{
                        if(typeof item['minlength'] != undefined )
                            if(elementForm.vale.length < item['minlength'])
                                itemval={
                                    type: 'text',
                                    status: false,
                                    msg: (typeof item['minlength']['msg'] != undefined )? item['minlength']['msg'] : lang["msgminlength"]
                                };
                                break;     
                        if(typeof item['maxlength'] != undefined )
                            if(elementForm.vale.length > item['minlength'])
                                itemval={
                                    type: 'text',
                                    status: false,
                                    msg: (typeof item['maxlength']['msg'] != undefined )? item['maxlength']['msg'] : lang["msgmaxlength"]
                                };
                                break;
                    }
                }
            }
        }
    }
    return itemval;
}